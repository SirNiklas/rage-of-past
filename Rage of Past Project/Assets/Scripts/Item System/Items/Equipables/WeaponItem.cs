﻿using UnityEngine;
using System.Collections;

public class WeaponItem : EquipableItem
{
    public int damage;
    public float attackSpeed;
}
