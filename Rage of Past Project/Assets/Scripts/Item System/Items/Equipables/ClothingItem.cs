﻿using UnityEngine;
using System.Collections;
using System;

public class ClothingItem : EquipableItem
{
    public ClothingType type;
    public int armor;

    public string onEquipBuffName;
    public BaseBuff onEquipBuff;

    public override void OnEquip()
    {
        if (owner == null)
        {
            Debug.LogWarning("This item has no owner but gets equipped!");
            return;
        }

        owner.GetAttribute(AttributeType.BonusArmor).value += armor;
        if (onEquipBuffName != null)
            owner.ApplyBuff(onEquipBuffName);

        base.OnEquip();
    }
    public override void OnUnequip()
    {
        if (owner == null)
        {
            Debug.LogWarning("This item has no owner but gets unequipped!");
            return;
        }

        owner.GetAttribute(AttributeType.BonusArmor).value += armor;
        if (onEquipBuffName != null)
            owner.UnapplyBuff(onEquipBuffName, true);

        base.OnUnequip();
    }
}

[Serializable]
public enum ClothingType
{
    Helmet,
    Bracers,
    Glove,
    Cuirass,
    LegPiece,
    Shoe
}
