﻿using UnityEngine;
using System.Collections;

public class EquipableItem : BaseItem
{
    public bool isEquipped;

    public virtual void OnEquip()
    {
        isEquipped = true;
    }
    public virtual void OnUnequip()
    {
        isEquipped = false;
    }
}
