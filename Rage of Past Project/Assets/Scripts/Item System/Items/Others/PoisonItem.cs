﻿using UnityEngine;
using System.Collections;

// Tp poison weapon items that affect foes
public class PoisonItem : BaseItem
{
    public int hitDuration; // After how much hits is the poison washed away?
    public string effectBuff;
}
