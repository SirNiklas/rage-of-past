﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

public abstract class BaseItem : ScriptableObject
{
    public string itemName, itemDescription;
    public int goldWorth;
    public float weight;
    [JsonIgnore]
    public GameObject gameObject;
    [JsonIgnore]
    public ActorUnit owner;

    public virtual void OnAddToInventory(Inventory inventory)
    {
        owner = inventory.inventoryActor;
    }
    public virtual void OnRemoveFromInventory()
    {
        owner = null;
    }

    public static BaseItem GetItem(string name)
    {
        return UnityEngine.Object.Instantiate(Resources.Load("Items/" + name, typeof(BaseItem))) as BaseItem;
    }
}

public enum ItemGroup
{
    Belare
}
