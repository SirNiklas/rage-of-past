﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

[Serializable]
public class SavePackage
{
    public const string savePackageFolder = @"saves\", savePackageFileExtension = ".ropsave", tempCurrentSaveFilePath = "temp_currentsave";

    public string name;
    public string version;
    public DateTime creationTimestamp;

    public List<string> acceptedQuests;
    public List<string> fulfilledQuests;
    // TODO: Save the current time and day

    public List<SavePackagePair> units;

    public SavePackage()
    { }
    public SavePackage(string _name, string _version, DateTime _creationtimestamp)
    {
        name = _name;
        version = _version;
        creationTimestamp = _creationtimestamp;

        acceptedQuests = new List<string>();
        fulfilledQuests = new List<string>();
        units = new List<SavePackagePair>();
    }

    public void ApplyToWorld()
    {
        if (version != GameManager.version)
            return;

        QuestManager.Instance.acceptedQuests.Clear();
        foreach (var quest in acceptedQuests)
        {
            QuestManager.Instance.AddQuest(Quest.GetQuest(quest));
        }
        QuestManager.Instance.fulfilledQuests.Clear();
        foreach (var quest in fulfilledQuests)
        {
            QuestManager.Instance.fulfilledQuests.Add(quest);
        }

        List<BaseUnit> worldUnitsRest = new List<BaseUnit>();
        worldUnitsRest.AddRange(BaseUnit.units);

        foreach (var savedunit in units)
        {
            for (int i = 0; i < BaseUnit.units.Count; i++)
            {
                BaseUnit worldunit = BaseUnit.units[i];

                if (savedunit.permanentId == worldunit.permanentId)
                {
                    worldUnitsRest.Remove(worldunit);
                    worldunit.OnLoadSave(savedunit);
                }
            }      
        }

        for (int i = 0; i < worldUnitsRest.Count; i++)
        {
            worldUnitsRest[i].Destroy();
        }
    }
    public void GetFromWorld()
    {
        foreach (var quest in QuestManager.Instance.acceptedQuests)
        {
            acceptedQuests.Add(quest.questName);
        }
        foreach (var quest in QuestManager.Instance.fulfilledQuests)
        {
            fulfilledQuests.Add(quest);
        }

        foreach (var worldunit in BaseUnit.units)
        {
            SavePackagePair pair = new SavePackagePair(worldunit.permanentId);
            worldunit.OnCreateSave(pair);
            units.Add(pair);
        }
    }

    public static void Load(string name)
    {
        if (!File.Exists(savePackageFolder + name + savePackageFileExtension))
            return;

        using (StreamWriter sw = new StreamWriter(File.Create(tempCurrentSaveFilePath)))
        {
            sw.WriteLine(name);
            Application.LoadLevel(Application.loadedLevelName);
        }
    }
    public static void Create(string name)
    {
        using (StreamWriter sw = new StreamWriter(File.Create(savePackageFolder + name + savePackageFileExtension)))
        {
            SavePackage savePackage = new SavePackage(name, GameManager.version, DateTime.Now);
            savePackage.GetFromWorld();

            sw.Write(JsonConvert.SerializeObject(savePackage, Formatting.Indented));
        }
    }
}

[Serializable]
public class SavePackagePair
{
    public string permanentId;
    public Dictionary<string, object> data;

    public SavePackagePair()
    { }
    public SavePackagePair(string _uniqueid)
    {
        permanentId = _uniqueid;
        data = new Dictionary<string, object>();
    }
}
