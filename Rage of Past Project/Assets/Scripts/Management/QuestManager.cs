﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestManager : MonoBehaviour
{
    public static QuestManager Instance;
    public List<Quest> acceptedQuests;
    public List<string> fulfilledQuests;

    public void AddQuest(Quest quest)
    {
        acceptedQuests.Add(quest);
    }
    public void RemoveQuest(Quest quest)
    {
        if (acceptedQuests.Contains(quest))
            acceptedQuests.Remove(quest);
    }

    public void OnQuestFulfilled(Quest quest)
    {
        RemoveQuest(quest);
        fulfilledQuests.Add(quest.questName);
    }

    void Awake()
    {
        Instance = this;
    }
}
