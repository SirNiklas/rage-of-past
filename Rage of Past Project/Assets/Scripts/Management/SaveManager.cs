﻿using UnityEngine;
using System.Collections;
using System.IO;
using Newtonsoft.Json;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Instance;

    void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        if (File.Exists(SavePackage.tempCurrentSaveFilePath))
        {
            string currentSaveName = "";
            using (StreamReader sr = new StreamReader(File.Open(SavePackage.tempCurrentSaveFilePath, FileMode.Open)))
            {
                currentSaveName = sr.ReadLine();
            }

            string filePath = (SavePackage.savePackageFolder + currentSaveName + SavePackage.savePackageFileExtension).Replace("\n", "");

            using (StreamReader sr = new StreamReader(File.Open(filePath, FileMode.Open)))
            {
                SavePackage savePackage = JsonConvert.DeserializeObject<SavePackage>(sr.ReadToEnd());
                savePackage.ApplyToWorld();
            }
        }
    }
    void OnApplicationQuit()
    {
        File.Delete(SavePackage.tempCurrentSaveFilePath);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            SavePackage.Create("New Save");
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            SavePackage.Load("New Save");
        }
    }
}
