﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class Inventory
{
    [JsonIgnore]
    public ActorUnit inventoryActor;
    [JsonIgnore]
    public List<string> startingItems;
    [JsonIgnore]
    public List<BaseItem> items;

    public void AddToInventory(BaseItem item)
    {
        if (items.Contains(item))
            return;

        item.OnAddToInventory(this);
        items.Add(item);
    }
    public void RemoveFromInventory(BaseItem item)
    {
        if (!items.Contains(item))
            return;

        item.OnRemoveFromInventory();
        items.Remove(item);
    }

    public BaseItem FindItem(string itemname)
    {
        foreach (var item in items)
        {
            if (item.itemName == itemname)
                return item;
        }

        return null;
    }
}
