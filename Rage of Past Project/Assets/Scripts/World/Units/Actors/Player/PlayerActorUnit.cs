﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;
using System;

public class PlayerActorUnit : ActorUnit
{
    public static PlayerActorUnit Instance;

    public Level level;
    public NPCActorUnit talkTarget;
    public int usableLevelPoints;

    public override void Awake()
    {
        Instance = this;
        level.AddXp(1);

        base.Awake();
    }
    public override void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            ReceiveDamage(10, DamageType.Physical, this);
        }

        base.Update();
    }

    public override void OnCreateSave(SavePackagePair save)
    {
        save.data.Add("level", level);
        save.data.Add("usableLevelPoints", usableLevelPoints);

        base.OnCreateSave(save);
    }
    public override void OnLoadSave(SavePackagePair save)
    {
        level = JsonConvert.DeserializeObject<Level>(save.data["level"].ToString());
        usableLevelPoints = int.Parse(save.data["usableLevelPoints"].ToString());
        base.OnLoadSave(save);
    }

    public void TalkTo(NPCActorUnit npc)
    {
        if (isTalking)
            return;

        isTalking = true;
        talkTarget = npc;
        talkTarget.TalkToPlayer();
    }
    public void Talk(DialogOption dialogoption)
    {
        if (!isTalking)
            return;

        talkTarget.dialog.OnPlayerTalk(dialogoption);
        talkTarget.Talk(dialogoption);
    }
    public void StopTalking()
    {
        isTalking = false;
        talkTarget.StopTalking();
        talkTarget = null;
    }

    public void OnLevelUp()
    {
        usableLevelPoints += 1;
    }
}

[Serializable]
public class Level
{
    public const int maxLevel = 100, neededXpMultiplicator = 200;
    public int currentLevel, currentXp, neededXp;

    public void AddXp(int value)
    {
        currentXp += value;
        if (currentLevel >= neededXp && currentLevel < 100)
        {
            PlayerActorUnit.Instance.OnLevelUp();

            currentLevel += 1;
            neededXp = currentLevel * neededXpMultiplicator;
        }
    }
}
