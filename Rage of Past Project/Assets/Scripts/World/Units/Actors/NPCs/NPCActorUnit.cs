﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

public class NPCActorUnit : ActorUnit
{
    public const float playerNPCEnemySearchDistance = 120, enemyOutOfVisionMaxTime = 5,
        walkMovementSpeed = 5f, runMovementSpeed = 8f, crouchMovementSpeed = 3f; // Defines when the npc enemy search-system should start

    public int level;
    public NavMeshAgent navMeshAgent;
    //public NPCMoodType mood; // Is this needed?
    public bool isAggressive, isInFightingMode;

    public NPCAction currentAction;
    public ActorUnit enemyTarget;
    public Vector3 walkTarget, enemySearchTarget;

    public float dayTimeVision, nightTimeVision;
    public Dialog dialog;

    public bool hasWalkTarget { get { return walkTarget != Vector3.zero; } }
    public int totalLevel { get { return level + Mathf.RoundToInt(level * PlayerActorUnit.Instance.level.currentLevel / 5); } }

    public float searchEnemiesDeltaTime, enemyOutOfVisionTime;
    private Ray seeActorRay;
    private RaycastHit seeActorRaycastHit;

    public override void Awake()
    {
        dialog.dialogActor = this;
        base.Awake();
    }
    public override void Update()
    {
        AdjustAttributes();

        if (enemyTarget == null)
        {
            if (hasWalkTarget && !isInFightingMode)
                MoveTo(walkTarget);
        }
        else
        {
            if (enemyTarget.isDead)
            {
                SetEnemyTarget(null);
                return;
            }

            if (isInFightingMode && !CanSeeActor(enemyTarget))
                enemyOutOfVisionTime += Time.deltaTime;
        }
        if (enemyOutOfVisionTime >= enemyOutOfVisionMaxTime)
        {
            SetEnemyTarget(null);
            enemyOutOfVisionTime = 0;
        }

        if (isAggressive && Vector3.Distance(PlayerActorUnit.Instance.transform.position, this.transform.position) <= playerNPCEnemySearchDistance)
        {
            searchEnemiesDeltaTime += Time.deltaTime;
            if (searchEnemiesDeltaTime >= 0.5f)
            {
                searchEnemiesDeltaTime = 0;

                List<ActorUnit> enemiesInRange = new List<ActorUnit>();
                // TODO: When daytime/nighttime exists use it here
                Collider[] cols = Physics.OverlapSphere(this.transform.position, /*TimeManager.isDay ? */dayTimeVision/* : nightTimeVision*/);
                foreach (var col in cols)
                {
                    if (col.CompareTag("Actor"))
                    {
                        ActorUnit actor = col.GetComponent<ActorUnit>();
                        if (actor != null)
                        {
                            if (actor.race != this.race && actor != this)
                                enemiesInRange.Add(actor);
                        }
                    }
                }

                OnSearchEnemies(enemiesInRange);
            }
        }

        base.Update();
    }
    public override void ReceiveDamage(int value, DamageType type, ActorUnit actor)
    {
        isInFightingMode = true;
        if (enemyTarget == null)
            MoveTo(actor.transform.position); // Walk to the position where the attacker stood
        base.ReceiveDamage(value, type, actor);
    }
    public override void OnCreateSave(SavePackagePair save)
    {
        save.data.Add("isAggressive", isAggressive);
        save.data.Add("isInFightingMode", isInFightingMode);
        save.data.Add("walkTarget", walkTarget);

        base.OnCreateSave(save);
    }
    public override void OnLoadSave(SavePackagePair save)
    {
        isAggressive = bool.Parse(save.data["isAggressive"].ToString());
        isInFightingMode = bool.Parse(save.data["isInFightingMode"].ToString());
        walkTarget = JsonConvert.DeserializeObject<Vector3>(save.data["walkTarget"].ToString());

        base.OnLoadSave(save);
    }

    public override void SetMovement(MovementType m)
    {
        GetAttribute(AttributeType.BonusMovementspeed).value = (int)(m == MovementType.Walk ? walkMovementSpeed : (m == MovementType.Run ? runMovementSpeed : crouchMovementSpeed));
        base.SetMovement(m);
    }

    public virtual void OnSearchEnemies(List<ActorUnit> enemiesinrange)
    {
        if (enemyTarget == null && enemiesinrange.Count > 0)
        {
            foreach (var enemy in enemiesinrange)
            {
                if (!enemy.isDead && CanSeeActor(enemy) && enemy.race != race)
                    SetEnemyTarget(enemy);
            }
        }
    }

    public void TalkToPlayer()
    {
        if (isTalking)
            return;

        isTalking = true;
        PlayerActorUnit.Instance.isTalking = true;
    }
    public void StopTalking()
    {
        if (!isTalking)
            return;

        isTalking = false;
        PlayerActorUnit.Instance.isTalking = false;
    }

    public void Talk(DialogOption dialogoption)
    {
        audioSource.PlayOneShot(dialogoption.answerAudio);
        // TODO: Show subtitles?
    }
    public void TurnTo(Vector3 position)
    {
        // TODO: Play a turn animation and rotate
    }
    public void SetEnemyTarget(ActorUnit target)
    {
        enemyTarget = target;
        if (enemyTarget == null)
        {
            isInFightingMode = false;
            TogglePutWeaponInHand(false);
            return;
        }
        else
        {
            isInFightingMode = true;
            StopTalking();
            TogglePutWeaponInHand(true);
        }
    }
    public void SetWalkTarget(Vector3 position)
    {
        walkTarget = position;
        if (!hasWalkTarget)
            return;

        MoveTo(walkTarget);
    }
    public void MoveTo(Vector3 position)
    {
        navMeshAgent.SetDestination(position);
    }

    private void AdjustAttributes()
    {
        GetAttribute(AttributeType.BaseMaxHealth).value = baseHealth + totalLevel;
        GetAttribute(AttributeType.BaseMaxMana).value = baseMana + totalLevel;
        GetAttribute(AttributeType.BaseMaxStamina).value = baseStamina + totalLevel;
    }
    private bool CanSeeActor(ActorUnit actor)
    {
        seeActorRay.origin = transform.position;
        seeActorRay.direction = (actor.transform.position - transform.position).normalized;
        if (Physics.Raycast(seeActorRay, out seeActorRaycastHit, Mathf.Infinity))
        {
            if (seeActorRaycastHit.collider.gameObject.GetComponent<ActorUnit>() == actor)
                return true;
            else return false;
        }
        else return false;
    }

    public void SetAction(NPCAction action)
    {
        currentAction = action;
        if (currentAction != null)
            currentAction.SetAction(this);
    }
}

[Serializable]
public class NPCAction
{
    public string name;
    public NPCActionType actionType;

    public int specificAnimationNumber;
    public ActorUnit attackActorReference;
    public Vector3 walkRunTarget;

    public void SetAction(NPCActorUnit affectedunit)
    {
        switch (actionType)
        {
            case NPCActionType.Attack:
                affectedunit.SetEnemyTarget(attackActorReference);
                break;
            case NPCActionType.WalkTo:
                affectedunit.SetMovement(MovementType.Walk);
                affectedunit.SetWalkTarget(walkRunTarget);
                break;
            case NPCActionType.RunTo:
                affectedunit.SetMovement(MovementType.Run);
                affectedunit.SetWalkTarget(walkRunTarget);
                break;
            case NPCActionType.CrouchTo:
                affectedunit.SetMovement(MovementType.Crouch);
                affectedunit.SetWalkTarget(walkRunTarget);
                break;
            case NPCActionType.PlaySpecificAnimation:
                // TODO: Set the specific animation
                break;
            default:
                break;
        }
    }
}
[Serializable]
public enum NPCActionType
{
    Attack,
    WalkTo,
    RunTo,
    CrouchTo,
    PlaySpecificAnimation
}
//[Serializable]
//public enum NPCMoodType
//{
//    VeryGood,
//    Good,
//    Moderate,
//    NotGood,
//    Angry
//}
