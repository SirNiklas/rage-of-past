﻿using UnityEngine;
using System.Collections;

public class AnimalNPCActor : NPCActorUnit
{
    public int directAttackDamage;

    public override void Update()
    {
        if (enemyTarget != null)
        {
            MoveTo(enemyTarget.transform.position);
            if (Vector3.Distance(this.transform.position, enemyTarget.transform.position) < 2)
                Attack();
        }
        base.Update();
    }
}
