﻿using UnityEngine;
using System.Collections;

public class HumanoidNPCActor : NPCActorUnit
{
    public string equippedWeapon;

    public override void Start()
    {
        WeaponItem w = inventory.FindItem(equippedWeapon) as WeaponItem;
        if (w != null)
            EquipWeapon(w);

        base.Start();
    }

    public override void Update()
    {
        if(enemyTarget != null)
        {
            if (currentWeapon is BowWeaponItem || currentWeapon is StaffWeaponItem)
            {
                Attack();
            }
            else
            {
                MoveTo(enemyTarget.transform.position);
                if (Vector3.Distance(this.transform.position, enemyTarget.transform.position) < 2)
                    Attack();
            }
        }

        base.Update();
    }
}
