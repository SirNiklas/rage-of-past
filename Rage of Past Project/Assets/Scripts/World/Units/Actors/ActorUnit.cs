﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

// Represents a humanoid unit, able to walk and interact
public abstract class ActorUnit : BaseUnit
{
    public string actorName;
    public ActorRace race;

    public bool isDead, isInvulnerable, isTalking;
    public MovementType movement;
    public Inventory inventory;
    public List<BaseBuff> buffs;
    public List<Effect> effects;
    public LocationEventUnit currentLocation;

    public int baseHealth, baseHealthRegeneration, baseMana, baseManaRegeneration,
        baseStamina, baseStaminaRegeneration, baseMagicResistance, baseMovementspeed, baseArmor;

    public Transform effectGameObjectTransform;
    public Animator actorAnimator;
    public AudioSource audioSource;

    #region Weapon
    public Transform weaponTransform;
    public WeaponItem currentWeapon;
    public bool isWeaponInHand;
    public bool isWeaponEquipped { get { return currentWeapon != null; } }
    #endregion

    #region Clothes
    public Transform helmetTransform, bracerLeftTransform, bracerRightTransform, gloveLeftTransform, gloveRightTransform, cuirassTransform, legPiecesTransform, shoeLeftTransform, shoeRightTransform;
    public ClothingItem helmet, bracers, gloves, cuirass, legPieces, shoes;
    #endregion

    public List<AttributeValuePair> attributes;
    #region Attribute shortcuts
    public int totalMaxHealth
    {
        get { return GetAttribute(AttributeType.BaseMaxHealth).value + GetAttribute(AttributeType.BonusMaxHealth).value; }
    }
    public int totalMaxMana
    {
        get { return GetAttribute(AttributeType.BaseMaxMana).value + GetAttribute(AttributeType.BonusMaxMana).value; }
    }
    public int totalHealthRegeneration
    {
        get { return GetAttribute(AttributeType.BaseHealthRegeneration).value + GetAttribute(AttributeType.BonusManaRegeneration).value; }
    }
    public int totalManaRegeneration
    {
        get { return GetAttribute(AttributeType.BaseManaRegeneration).value + GetAttribute(AttributeType.BonusManaRegeneration).value; }
    }
    public int totalStaminaRegeneration
    {
        get { return GetAttribute(AttributeType.BaseStaminaRegeneration).value + GetAttribute(AttributeType.BonusStaminaRegeneration).value; }
    }
    public int totalArmor
    {
        get { return GetAttribute(AttributeType.BaseArmor).value + GetAttribute(AttributeType.BonusArmor).value; }
    }
    public int totalMagicResistance
    {
        get { return GetAttribute(AttributeType.BaseMagicResistance).value + GetAttribute(AttributeType.BonusMagicResistance).value; }
    }
    public int totalArmorPercentage
    {
        get { return (int)((0.05f * (float)totalArmor) / (1 + 0.06f * (float)totalArmor) * 100); }
    }
    #endregion

    private float regenerationDeltaTime;

    public AttributeValuePair GetAttribute(AttributeType type)
    {
        return attributes.Find((a) => { if (a.type == type) return true; else return false; });
    }

    public override void Awake()
    {
        inventory.inventoryActor = this;

        // Health
        attributes.Add(new AttributeValuePair(AttributeType.BaseMaxHealth, baseHealth));
        attributes.Add(new AttributeValuePair(AttributeType.BonusMaxHealth, 0));
        attributes.Add(new AttributeValuePair(AttributeType.BaseHealthRegeneration, baseHealthRegeneration));
        attributes.Add(new AttributeValuePair(AttributeType.BonusHealthRegeneration, 0));
        attributes.Add(new AttributeValuePair(AttributeType.CurrentHealth, baseHealth));
        // Mana
        attributes.Add(new AttributeValuePair(AttributeType.BaseMaxMana, baseMana));
        attributes.Add(new AttributeValuePair(AttributeType.BonusMaxMana, 0));
        attributes.Add(new AttributeValuePair(AttributeType.BaseManaRegeneration, baseManaRegeneration));
        attributes.Add(new AttributeValuePair(AttributeType.BonusManaRegeneration, 0));
        attributes.Add(new AttributeValuePair(AttributeType.CurrentMana, baseMana));
        // Stamina
        attributes.Add(new AttributeValuePair(AttributeType.BaseMaxStamina, baseStamina));
        attributes.Add(new AttributeValuePair(AttributeType.BonusMaxStamina, 0));
        attributes.Add(new AttributeValuePair(AttributeType.BaseStaminaRegeneration, baseStaminaRegeneration));
        attributes.Add(new AttributeValuePair(AttributeType.BonusStaminaRegeneration, 0));
        attributes.Add(new AttributeValuePair(AttributeType.CurrentStamina, baseStamina));
        // Others
        attributes.Add(new AttributeValuePair(AttributeType.BaseArmor, baseArmor));
        attributes.Add(new AttributeValuePair(AttributeType.BonusArmor, 0));
        attributes.Add(new AttributeValuePair(AttributeType.BaseMovementspeed, baseMovementspeed));
        attributes.Add(new AttributeValuePair(AttributeType.BonusMovementspeed, 0));
        attributes.Add(new AttributeValuePair(AttributeType.BaseMagicResistance, baseMagicResistance));
        attributes.Add(new AttributeValuePair(AttributeType.BonusMovementspeed, 0));

        foreach (var inventorystartingitem in inventory.startingItems)
        {
            inventory.AddToInventory(BaseItem.GetItem(inventorystartingitem));
        }

        base.Awake();
    }
    public override void Update()
    {
        if (isDead)
        {

            return;
        }

        CheckDeadState();
        Regenerate();
        UpdateBuffs();

        base.Update();
    }

    public override void OnCreateSave(SavePackagePair save)
    {
        save.data.Add("attributes", attributes);
        save.data.Add("isDead", isDead);
        save.data.Add("isInvulnerable", isInvulnerable);
        save.data.Add("buffs", buffs);

        save.data.Add("inventory.items", inventory.items);

        save.data.Add("currentWeapon", isWeaponEquipped ? currentWeapon.name : null);
        save.data.Add("isWeaponInHand", isWeaponInHand);

        save.data.Add("movement", movement);

        base.OnCreateSave(save);
    }
    public override void OnLoadSave(SavePackagePair save)
    {
        attributes = JsonConvert.DeserializeObject<List<AttributeValuePair>>(save.data["attributes"].ToString());
        isDead = bool.Parse(save.data["isDead"].ToString());
        isInvulnerable = bool.Parse(save.data["isInvulnerable"].ToString());

        movement = (MovementType)Enum.Parse(typeof(MovementType), save.data["movement"].ToString());

        // Remove all effects
        foreach (var effect in effects)
        {
            RemoveEffect(effect.name);
        }
        // No effects need to be saved, they get automatically reapplied with the buff applies

        List<BaseBuff> savedBuffs = JsonConvert.DeserializeObject<List<BaseBuff>>(save.data["buffs"].ToString());
        // Remove all buffs properly
        foreach (var buff in buffs)
        {
            UnapplyBuff(buff, true);
        }
        // Apply all saved buffs with a special method (adds effects automatically)
        foreach (var buff in savedBuffs)
        {
            ApplySavedBuff(buff);
        }

        List<string> savedInventoryItems = JsonConvert.DeserializeObject<List<string>>(save.data["inventory.items"].ToString());
        inventory.items.Clear();
        foreach (var item in savedInventoryItems)
        {
            inventory.AddToInventory(BaseItem.GetItem(item));
        }

        // TODO: Find the weapon item in the inventory and equip it by EquipWeapon()
        if (save.data["currentWeapon"] == null)
            EquipWeapon(null);
        else
            EquipWeapon((WeaponItem)inventory.FindItem(save.data["currentWeapon"].ToString()));

        isWeaponInHand = bool.Parse(save.data["isWeaponInHand"].ToString());

        base.OnLoadSave(save);
    }

    public virtual void ReceiveDamage(int value, DamageType type, ActorUnit actor)
    {
        int totalDamage = value;
        if (type == DamageType.Physical)
            totalDamage = Mathf.RoundToInt(totalDamage - (totalDamage / 100 * totalArmor));
        else if (type == DamageType.Magical)
            totalDamage = Mathf.RoundToInt(totalDamage - (totalDamage/ 100 * totalMagicResistance));
        // Poison damage is pure, do nothing

        GetAttribute(AttributeType.CurrentHealth).value -= totalDamage;
        if (GetAttribute(AttributeType.CurrentHealth).value < 0)
            GetAttribute(AttributeType.CurrentHealth).value = 0;
    }

    public void ReceiveHeal(int value)
    {
        GetAttribute(AttributeType.CurrentHealth).value += value;
        if (GetAttribute(AttributeType.CurrentHealth).value > totalMaxHealth)
            GetAttribute(AttributeType.CurrentHealth).value = totalMaxHealth;
    }
    public void ReceiveMana(int value)
    {
        GetAttribute(AttributeType.CurrentMana).value += value;
        if (GetAttribute(AttributeType.CurrentMana).value > totalMaxHealth)
            GetAttribute(AttributeType.CurrentMana).value = totalMaxHealth;
    }
    public void ReceiveStamina(int value)
    {
        GetAttribute(AttributeType.CurrentStamina).value += value;
        if (GetAttribute(AttributeType.CurrentStamina).value > totalMaxHealth)
            GetAttribute(AttributeType.CurrentStamina).value = totalMaxHealth;
    }

    public virtual void SetMovement(MovementType m)
    {
        movement = m;
    }

    public BaseBuff ApplyBuff(string name)
    {
        BaseBuff b = BaseBuff.GetBuff(name);
        foreach (var buff in buffs)
        {
            if (buff.buffName == b.buffName)
            {
                // The buff that should get applied already exists
                // Choice 1: If the buff isnt stackable the already existing buff gets its livetime reset
                // Choice 2: If this buff is stackable we just add the new buff

                if (buff.isStackable)
                {
                    b.OnApplyBuff(this, false);
                    buffs.Add(b);
                }
                else
                {
                    buff.livetime = buff.currentLivetime;
                    return null;
                }
            }
        }

        return b;
    }
    public void UnapplyBuff(BaseBuff buff, bool force)
    {
        if (buffs.Contains(buff))
        {
            buff.OnUnapplyBuff();

            if (buff.isRemovable)
                buffs.Remove(buff);
            else if (force)
                buffs.Remove(buff);
        }
    }
    public void UnapplyBuff(string buffname, bool force)
    {
        BaseBuff buff = buffs.Find((b) => { if (b.buffName == buffname) return true; else return false; });
        if (buff == null)
            return;

        if (buffs.Contains(buff))
        {
            buff.OnUnapplyBuff();

            if (buff.isRemovable)
                buffs.Remove(buff);
            else if (force)
                buffs.Remove(buff);
        }
    }
    public void ClearAllBuffs(bool force)
    {
        foreach (var buff in buffs)
        {
            UnapplyBuff(buff, force);
        }
    }

    public Effect AddEffect(string name)
    {
        Effect e = Effect.GetEffect(name);
        e.OnAddEffect(this);
        effects.Add(e);

        return e;
    }
    public void RemoveEffect(string name)
    {
        Effect e = null;
        foreach (var effect in effects)
        {
            if (effect.name == name)
                e = effect;
        }

        if (e != null)
        {
            foreach (var instantiatedeffect in e.instantiatedEffectNames)
            {
                Destroy(effectGameObjectTransform.FindChild(instantiatedeffect).gameObject);
            }
            e.OnRemoveEffect();

            effects.Remove(e);
        }
    }
    public void ClearAllEffects()
    {
        foreach (var effect in effects)
        {
            RemoveEffect(effect.effectName);
        }
    }

    public void EquipWeapon(WeaponItem weapon)
    {
        currentWeapon = weapon;
    }
    public void TogglePutWeaponInHand(bool value)
    {
        isWeaponInHand = value;
    }
    public void EquipClothing(ClothingItem cloth)
    {
        if (cloth.isEquipped)
            return;

        ClothingItem c = null;
        switch (cloth.type)
        {
            case ClothingType.Helmet:
                if (helmet != null)
                    helmet.OnUnequip();
                c = helmet;
                break;
            case ClothingType.Glove:
                if (gloves != null)
                    gloves.OnUnequip();
                c = gloves;
                break;
            case ClothingType.Bracers:
                if (bracers != null)
                    bracers.OnUnequip();
                c = bracers;
                break;
            case ClothingType.Cuirass:
                if (cuirass != null)
                    cuirass.OnUnequip();
                c = cuirass;
                break;
            case ClothingType.LegPiece:
                if (legPieces != null)
                    legPieces.OnUnequip();
                c = legPieces;
                break;
            case ClothingType.Shoe:
                if (shoes != null)
                    shoes.OnUnequip();
                c = shoes;
                break;
        }

        if(c != null)
        {
            c.OnEquip();
        }
    }

    public void Attack()
    {
        if (isWeaponEquipped)
        {
            // TODO: Play animation and hit with weapon
        }
    }

    private void Regenerate()
    {
        regenerationDeltaTime += Time.deltaTime;
        if (regenerationDeltaTime >= 1)
        {
            regenerationDeltaTime = 0;
            ReceiveHeal(totalHealthRegeneration);
            ReceiveMana(totalManaRegeneration);
            ReceiveStamina(totalStaminaRegeneration);
        }
    }
    private void UpdateBuffs()
    {
        for (int i = 0; i < buffs.Count; i++)
        {
            buffs[i].OnUpdate(); // TODO: When the buff recognizes that its lifetime is over, it removes itself and the whole list gets reduced so the index is wrong, change this maybe?
        }
    }
    private void CheckDeadState()
    {
        if (GetAttribute(AttributeType.CurrentHealth).value <= 0)
        {
            GetAttribute(AttributeType.CurrentHealth).value = 0;
            isDead = true;
        }
    }
    private void ApplySavedBuff(BaseBuff buff)
    {
        if (buffs.Contains(buff))
            return;

        buff.OnApplyBuff(this, true);
        buffs.Add(buff);
    }
}

[Serializable]
public class AttributeValuePair
{
    public AttributeType type;
    public int value;

    public AttributeValuePair()
    { }
    public AttributeValuePair(AttributeType _type, int _value)
    {
        type = _type;
        value = _value;
    }
}

[Serializable]
public enum AttributeType
{
    BaseMaxHealth,
    BonusMaxHealth,
    CurrentHealth,
    BaseHealthRegeneration,
    BonusHealthRegeneration,

    BaseMaxMana,
    BonusMaxMana,
    CurrentMana,
    BaseManaRegeneration,
    BonusManaRegeneration,

    BaseMaxStamina,
    BonusMaxStamina,
    CurrentStamina,
    BaseStaminaRegeneration,
    BonusStaminaRegeneration,    

    BaseArmor,
    BonusArmor,
    BaseMagicResistance,
    BonusMagicResistance,
    BaseMovementspeed,
    BonusMovementspeed,

}
[Serializable]
public enum ActorRace
{
    Belaren,
    Gremure,
    None
}
[Serializable]
public enum DamageType
{
    Physical,
    Magical,
    Poison
}
[Serializable]
public enum MovementType
{
    Walk,
    Run,
    Crouch
}