﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class EventUnit : BaseUnit
{
    public bool onlyReactToPlayer;
    public List<ActorUnit> actorsInArea;

    public virtual void OnActorEnterArea(ActorUnit actor)
    {

    }
    public virtual void OnActorExitArea(ActorUnit actor)
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Actor"))
        {
            ActorUnit actor = other.gameObject.GetComponent<ActorUnit>();
            if (actor != null)
            {
                actorsInArea.Add(actor);
                if (actor != PlayerActorUnit.Instance && onlyReactToPlayer)
                    return;
                else
                    OnActorEnterArea(actor);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Actor"))
        {
            ActorUnit actor = other.gameObject.GetComponent<ActorUnit>();
            if (actor != null)
            {
                actorsInArea.Remove(actor);
                if (actor != PlayerActorUnit.Instance && onlyReactToPlayer)
                    return;
                else
                    OnActorExitArea(actor);
            }
        }
    }
}
