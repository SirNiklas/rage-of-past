﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

public class LocationEventUnit : EventUnit
{
    public string locationName;
    public bool isDiscovered;

    public override void OnActorEnterArea(ActorUnit actor)
    {
        // TODO: Show a visual sign that this place has been discovered
        actor.currentLocation = this;
        isDiscovered = false;

        base.OnActorEnterArea(actor);
    }
    public override void OnActorExitArea(ActorUnit actor)
    {
        // If the current location name is still set on this one, set it to nothing
        // We NEED to check if it is still this one so overlapping areas dont cause problems
        if(actor.currentLocation == this)
            actor.currentLocation = null;

        base.OnActorExitArea(actor);
    }

    public override void OnCreateSave(SavePackagePair save)
    {
        save.data.Add("isDiscovered", isDiscovered);
        base.OnCreateSave(save);
    }
    public override void OnLoadSave(SavePackagePair save)
    {
        isDiscovered = bool.Parse(save.data["isDiscovered"].ToString());
        base.OnLoadSave(save);
    }
}
