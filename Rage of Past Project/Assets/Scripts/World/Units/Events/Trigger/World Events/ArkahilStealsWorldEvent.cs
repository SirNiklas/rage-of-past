﻿using UnityEngine;
using System.Collections;

public class ArkahilStealsWorldEvent : WorldEventUnit
{
    public GameObject arkahilGameObjectPrefab;

    public override void Activate()
    {
        Instantiate(arkahilGameObjectPrefab, transform.position, Quaternion.identity);
        base.Activate();
    }
    public override void OnEventStart()
    {
        FinishEvent();
        base.OnEventStart();
    }
}
