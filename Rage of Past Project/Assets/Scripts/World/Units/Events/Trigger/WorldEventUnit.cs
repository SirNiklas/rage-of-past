﻿using UnityEngine;
using System.Collections;

public class WorldEventUnit : EventUnit
{
    public const float checkConditionToActivateDistance = 100;
    public const float checkConditionToActivateMaxTime = 2;

    public bool isActivated, checkCondition;
    public Condition eventStartCondition;

    private float checkConditionToActivateDeltaTime;

    public override void Update()
    {
        if (Vector3.Distance(transform.position, PlayerActorUnit.Instance.transform.position) <= checkConditionToActivateDistance && !isActivated)
        {
            checkConditionToActivateDeltaTime += Time.deltaTime;
            if (checkConditionToActivateDeltaTime >= checkConditionToActivateMaxTime)
            {
                if (eventStartCondition.IsConditionFulfilled())
                    Activate();
            }
        }

        base.Update();
    }
    public override void OnActorEnterArea(ActorUnit actor)
    {
        // Is this right?
        if (isActivated)
            OnEventStart();
        base.OnActorEnterArea(actor);
    }
    public override void OnActorExitArea(ActorUnit actor)
    {
        base.OnActorExitArea(actor);
    }

    public override void OnCreateSave(SavePackagePair save)
    {
        save.data.Add("isActivated", isActivated);
        base.OnCreateSave(save);
    }
    public override void OnLoadSave(SavePackagePair save)
    {
        isActivated = bool.Parse(save.data["isActivated"].ToString());
        base.OnLoadSave(save);
    }

    public virtual void Activate()
    {
        isActivated = true;
    }
    public virtual void OnEventStart()
    {
        
    }

    public void FinishEvent()
    {
        Destroy();
    }
}
