﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

public abstract class BaseUnit : MonoBehaviour
{
    public static List<BaseUnit> units = new List<BaseUnit>();
    public string permanentId = Guid.NewGuid().ToString();

    public void Destroy()
    {
        Destroy(gameObject);
    }

    public virtual void Awake()
    {
        units.Add(this);
    }
    public virtual void OnDestroy()
    {
        units.Remove(this);
    }
    public virtual void Start()
    {
        
    }
    public virtual void Update()
    {
        
    }

    public virtual void OnCreateSave(SavePackagePair save)
    {
        save.data.Add("transform.position.x", transform.position.x);
        save.data.Add("transform.position.y", transform.position.y);
        save.data.Add("transform.position.z", transform.position.z);

        save.data.Add("transform.rotation.x", transform.rotation.x);
        save.data.Add("transform.rotation.y", transform.rotation.y);
        save.data.Add("transform.rotation.z", transform.rotation.z);
        save.data.Add("transform.rotation.w", transform.rotation.w);
    }
    public virtual void OnLoadSave(SavePackagePair save)
    {
        transform.position = new Vector3(
            float.Parse(save.data["transform.position.x"].ToString()),
            float.Parse(save.data["transform.position.y"].ToString()),
            float.Parse(save.data["transform.position.z"].ToString()));

        transform.rotation = new Quaternion(
            float.Parse(save.data["transform.rotation.x"].ToString()),
            float.Parse(save.data["transform.rotation.y"].ToString()),
            float.Parse(save.data["transform.rotation.z"].ToString()),
            float.Parse(save.data["transform.rotation.w"].ToString()));
    }
}
