﻿using UnityEngine;
using System.Collections;
using System;

using System.Xml.Serialization;
using Newtonsoft.Json;

public class WorldItemUnit : BaseUnit
{
    [JsonIgnore]
    public string resourceItemName;
    [JsonIgnore]
    public BaseItem item;

    public override void Awake()
    {
        item = BaseItem.GetItem(resourceItemName);
        base.Awake();
    }

    public override void OnCreateSave(SavePackagePair save)
    {
        base.OnCreateSave(save);
    }
    public override void OnLoadSave(SavePackagePair save)
    {
        base.OnLoadSave(save);
    }
}
