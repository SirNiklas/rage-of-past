﻿using UnityEngine;
using System.Collections;

public class IcespikePoisonBuff : BaseBuff
{
    public int damage, movementSpeedSlowPercentage;
    [HideInInspector]
    public int currentMovementspeedSlow;

    public override void OnEverySecond()
    {
        affectedActor.ReceiveDamage(damage, DamageType.Magical, null);
        base.OnEverySecond();
    }

    public override void OnApplyBuff(ActorUnit affected, bool isreapply)
    {
        currentMovementspeedSlow = affectedActor.GetAttribute(AttributeType.BonusMovementspeed).value / 100 * movementSpeedSlowPercentage;
        affectedActor.GetAttribute(AttributeType.BonusMovementspeed).value -= currentMovementspeedSlow;

        base.OnApplyBuff(affected, isreapply);
    }
    public override void OnUnapplyBuff()
    {
        affectedActor.GetAttribute(AttributeType.BonusMovementspeed).value += currentMovementspeedSlow;
        base.OnUnapplyBuff();
    }
}
