﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class BaseBuff : ScriptableObject
{
    public string buffName, buffDescription;
    [HideInInspector, JsonIgnore]
    public ActorUnit affectedActor;
    public float livetime, currentLivetime;
    public bool isRemovable, isStackable, hasLivetime, isInvisible;
    public List<string> effects;

    public bool isApplied { get { return affectedActor != null; } }

    private float onEverySecondDeltaTime;

    public virtual void OnApplyBuff(ActorUnit affected, bool isreapply)
    {
        affectedActor = affected;
        if(!isreapply)
            currentLivetime = livetime;

        foreach (var effect in effects)
        {
            affectedActor.AddEffect(effect);
        }
    }
    public virtual void OnUnapplyBuff()
    {
        foreach (var effect in effects)
        {
            affectedActor.RemoveEffect(effect);
        }

        affectedActor = null;
    }

    public virtual void OnUpdate()
    {
        onEverySecondDeltaTime += Time.deltaTime;
        if (onEverySecondDeltaTime >= 1 && isApplied)
        {
            // Check if the buff livetime is above 0
            if (hasLivetime)
            {
                currentLivetime -= 1;
                if (currentLivetime <= 0)
                {
                    affectedActor.UnapplyBuff(this, true);
                    return;
                }
            }

            onEverySecondDeltaTime = 0;
            OnEverySecond();
        }
    }
    public virtual void OnEverySecond()
    {

    }

    public static BaseBuff GetBuff(string name)
    {
        return UnityEngine.Object.Instantiate(Resources.Load("Buffs/" + name, typeof(BaseBuff))) as BaseBuff;
    }
}
