﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Condition
{
    // TODO: Add more conditions
    public string[] neededItemNames, solvedQuestsName;
    public string currentLocationName = "none", acceptedQuestName;
    public ActorRace actorRaceIs;
    public int minPlayerLevel = -1;

    public bool IsConditionFulfilled()
    {
        foreach (var neededitem in neededItemNames)
        {
            if (PlayerActorUnit.Instance.inventory.FindItem(neededitem) == null)
                return false;
        }
        if (acceptedQuestName != "")
        {
            bool contains = false;
            foreach (var quest in QuestManager.Instance.acceptedQuests)
            {
                if (quest.questName == acceptedQuestName)
                    contains = true;
            }
            if (!contains)
                return false;
        }
        if (minPlayerLevel != -1 && PlayerActorUnit.Instance.level.currentLevel < minPlayerLevel)
            return false;
        if (actorRaceIs != ActorRace.None && PlayerActorUnit.Instance.race != actorRaceIs)
            return false;
        if (currentLocationName != "none" && PlayerActorUnit.Instance.currentLocation.locationName == currentLocationName)
            return false;

        return true;
    }
}
