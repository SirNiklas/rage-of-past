﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Generic;

public class Effect : ScriptableObject
{
    public string effectName;
    [HideInInspector]
    public ActorUnit affectedUnit;

    public GameObject[] effectGameObjects;
    [HideInInspector]
    public List<string> instantiatedEffectNames;

    public void OnAddEffect(ActorUnit actor)
    {
        affectedUnit = actor;

        foreach (var effectGameObject in effectGameObjects)
        {
            GameObject effectGo = (GameObject)Instantiate(effectGameObject, actor.transform.position, Quaternion.identity);
            effectGo.transform.parent = affectedUnit.effectGameObjectTransform;
            effectGo.transform.position = Vector3.zero;
            effectGo.transform.localPosition = Vector3.zero;
            instantiatedEffectNames.Add(effectGo.name);
        }
    }
    public void OnRemoveEffect()
    {
        affectedUnit = null;
    }

    public static Effect GetEffect(string name)
    {
        return UnityEngine.Object.Instantiate(Resources.Load("Effects/" + name, typeof(Effect))) as Effect;
    }
}
