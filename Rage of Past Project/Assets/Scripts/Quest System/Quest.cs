﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Quest : ScriptableObject
{
    public string questName, questDescription;
    public List<QuestPart> questSections;
    public int currentQuestSection;
    public bool isQuestFulfilled;

    public void Update()
    {
        if (currentQuestSection < 0 || currentQuestSection > questSections.Count - 1)
            return;

        QuestPart part = questSections[currentQuestSection];
        if (part.questPartFulfilledCondition.IsConditionFulfilled())
        {
            if (currentQuestSection + 1 < questSections.Count - 1)
            {
                if (part.activatedWorldEvent != null)
                    part.activatedWorldEvent.Activate();
                currentQuestSection += 1;
            }
            else
                isQuestFulfilled = true;
        }
    }

    public static Quest GetQuest(string name)
    {
        return UnityEngine.Object.Instantiate(Resources.Load("Quests/" + name, typeof(Quest))) as Quest;
    }
}

[Serializable]
public class QuestPart
{
    public string questHeader;
    public bool isOptional;
    public Condition questPartFulfilledCondition;
    public WorldEventUnit activatedWorldEvent;
}
