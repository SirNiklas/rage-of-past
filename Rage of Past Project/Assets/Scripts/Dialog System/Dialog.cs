﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class Dialog : ScriptableObject
{
    public Condition conversationStartCondition;
    public List<DialogOption> startOptionsPlayer;
    public DialogOption nonConversationDialogOption;

    [HideInInspector]
    public NPCActorUnit dialogActor;
    [HideInInspector]
    public DialogOption lastDialogOption;

    public void OnPlayerTalk(DialogOption dialogoption)
    {
        lastDialogOption = dialogoption;
        lastDialogOption.OnAnswerSpokenEvent.Activate(dialogActor);
    }

    public static Dialog GetDialog(string name)
    {
        return UnityEngine.Object.Instantiate(Resources.Load("Dialogs/" + name, typeof(Dialog))) as Dialog;
    }
}

[Serializable]
public class DialogOption
{
    public string text, answer;
    public bool isOpenDialog;
    public AudioClip answerAudio;
    public DialogOptionEvent OnAnswerSpokenEvent;

    public Condition visibilityCondition;
    public List<DialogOption> options;
}

[Serializable]
public class DialogOptionEvent
{
    public string getNewQuestName, giveItemName, setQuestFinished;
    public int newExperience;
    public bool openInventoryShop, stopTalking;
    public NPCAction npcAction;

    public void Activate(NPCActorUnit owner)
    {
        PlayerActorUnit.Instance.level.AddXp(newExperience);
        if (getNewQuestName != "")
            QuestManager.Instance.AddQuest(Quest.GetQuest(getNewQuestName));
        if(giveItemName != "")
            PlayerActorUnit.Instance.inventory.AddToInventory(owner.inventory.FindItem(giveItemName));
        // TODO: Set quest finished
        // TODO: Open inventory for shopping
        if (npcAction.name != null)
            owner.SetAction(npcAction);
        if (stopTalking)
            owner.StopTalking();
    }
}
