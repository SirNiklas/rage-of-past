﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AttributeInfoGUI : MonoBehaviour
{
    public const float imageBarLength = 100;

    public GameObject attributeInfoGUIObject;
    public Image healthBar, staminaBar, manaBar; // Is a mana bar needed?

    public void ToggleVisiblity(bool value)
    {
        attributeInfoGUIObject.SetActive(value);
    }
    public void ToggleVisiblity(bool value, float time)
    {
        StopCoroutine("ToggleVisiblityTime");
        StartCoroutine(ToggleVisibilityTime(value, time));
    }

    void Start()
    {
        ToggleVisiblity(true);
    }
    void Update()
    {
        PlayerActorUnit p = PlayerActorUnit.Instance;
        //healthBar.rectTransform.rect.Set(healthBar.rectTransform.rect.xMin, healthBar.rectTransform.rect.yMin, (imageBarLength - 4) - ((((float)p.totalMaxHealth - (float)p.GetAttribute(AttributeType.CurrentHealth).value / p.totalMaxHealth) * (imageBarLength - 4))), healthBar.rectTransform.rect.height);

        // Update all image bars
    }

    IEnumerator ToggleVisibilityTime(bool value, float time)
    {
        ToggleVisiblity(value);
        yield return new WaitForSeconds(time);
        ToggleVisiblity(!value);
    }
}
