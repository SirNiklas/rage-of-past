﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogGUI : MonoBehaviour
{
    public GameObject dialogGUIObject;
    public Text dialogText, dialogNPCName;
    public Image dialogOptionsBox;

    void Update()
    {
        if(PlayerActorUnit.Instance.isTalking)
        {
            Dialog d = PlayerActorUnit.Instance.talkTarget.dialog;
            dialogText.text = d.lastDialogOption != null ? d.lastDialogOption.answer : "";
            dialogNPCName.text = d.dialogActor.actorName;

            if (!dialogGUIObject.activeSelf)
                dialogGUIObject.SetActive(true);
        }
        else
        {
            if (dialogGUIObject.activeSelf)
                dialogGUIObject.SetActive(false);
        }  
    }
}
