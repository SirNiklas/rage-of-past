﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class GUIScriptableObjectCreator
{
    [MenuItem("Resources/Create Buff (current: Icespike Poison)")]
    public static void CreateBuff()
    {
        ScriptableObject asset = ScriptableObject.CreateInstance(typeof(IcespikePoisonBuff));
        AssetDatabase.CreateAsset(asset, "Assets/Resources/Buffs/New Buff.asset");
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    [MenuItem("Resources/Create Dialog")]
    public static void CreateDialog()
    {
        ScriptableObject asset = ScriptableObject.CreateInstance(typeof(Dialog));
        AssetDatabase.CreateAsset(asset, "Assets/Resources/Dialogs/New Dialog.asset");
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    [MenuItem("Resources/Create Effect")]
    public static void CreateEffect()
    {
        ScriptableObject asset = ScriptableObject.CreateInstance(typeof(Effect));
        AssetDatabase.CreateAsset(asset, "Assets/Resources/Effects/New Effect.asset");
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    [MenuItem("Resources/Create Item (current: Belaren Onehand Sword)")]
    public static void CreateItem()
    {
        ScriptableObject asset = ScriptableObject.CreateInstance(typeof(BelarenOnehandSwordWeaponItem));
        AssetDatabase.CreateAsset(asset, "Assets/Resources/Items/New Item.asset");
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    [MenuItem("Resources/Create Quest")]
    public static void CreateQuest()
    {
        ScriptableObject asset = ScriptableObject.CreateInstance(typeof(Quest));
        AssetDatabase.CreateAsset(asset, "Assets/Resources/Quests/New Quest.asset");
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
